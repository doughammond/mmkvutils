#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

#include <MMKV.h>

void ensureDirectory(const std::string pathname)
{
	struct stat info;

	if (stat(pathname.c_str(), &info) != 0)
	{
		std::cerr << "cannot access " << pathname << std::endl;
		exit(1);
	}
	else if (!(info.st_mode & S_IFDIR))
	{
		std::cerr << "not a directory: " << pathname << std::endl;
		exit(1);
	}
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		std::cerr << "mmkv rootDir not specified" << std::endl;
		exit(1);
	}

	const auto rootDir = std::string(argv[1]);
	ensureDirectory(rootDir);

	MMKV::initializeMMKV(rootDir);
	auto mmkv = MMKV::defaultMMKV();
	for (auto &key : mmkv->allKeys())
	{
		std::cout << "\x1b[33m" << key << "\x1b[0m" << "\t";
		std::string item;
		mmkv->getString(key, item);
		std::cout << "\x1b[36m" << item << "\x1b[0m" << std::endl;
	}
}
